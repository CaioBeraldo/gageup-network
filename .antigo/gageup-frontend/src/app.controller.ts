import { Get, Controller, Post, Render } from '@nestjs/common';
import { AppService } from './app.service';
import { Client, Transport, ClientProxy, ClientProxyFactory } from '@nestjs/microservices';
import { Observable } from 'rxjs';

@Controller()
export class AppController {
  private readonly client: ClientProxy;

  constructor() {
    this.client = ClientProxyFactory.create({
      transport: Transport.TCP, options: {
        host: 'localhost',
        port: 8080,
      },
    });

    this.client.connect();
  }

  @Get()
  @Render('Login')
  async usuarios() {
    const pattern = { cmd: 'findAll' };
    const payload = {};
    const usuarios = await this.client.send<Array<any>>(pattern, payload).toPromise();

    return { usuarios };
    // : Observable<any[]>
    // return this.client.send<Array<any>>(pattern, payload);
  }

  @Get('findAll')
  async findAll() {
    const pattern = { cmd: 'findAll' };
    const payload = {};

    return await this.client.send<Array<any>>(pattern, payload);
    // : Observable<any[]>
    // return this.client.send<Array<any>>(pattern, payload);
  }

  @Get('incluir')
  insert(): Observable<any[]> {
    const pattern = { cmd: 'insert' };
    const payload = {
      nome: 'Caio Beraldo',
      email: 'caio.beraldo@fatec.sp.gov.br',
      userID: '1516546548479',
      accessToken: `EAADGuKw1TFwBAKJACT1lGp2uDMqSCUG9OBUM4yn1qYZCJA8IY8GOW8G6E4iq
bIGELMkZBzSajuZAfGSPbZAnnKClMJZB4YVZCAReZBzP4vbdoozXO3DQHHsxWQFRXxv44UZCoixgZCIZB
bYhoEKZBMMFZAxd27bqpzoyTLIAvpBfZBLp47xZBIKqHDzkUZCvr0DSv9vmFOnNTTwk6bFkQZDZD`,
      signedRequest: `8L3de6YlZz7Y860z5SHW6f9-GUH3lpRK_FCaTjQJLYw.eyJhbGdvcml0aG0
iOiJITUFDLVNIQTI1NiIsImNvZGUiOiJBUUI3ZEF4dFRDWlNVVFYwS01PeEczbmpKWkdrVEpSekVIQi11
WjR0WFV3c0MtcVdvblo5bGJINndtX0dBZ3dfLTBvUW13eWpNQlo0cGJMaVRQd1dQZ004bkVzWWw1Qm90W
nZpYk9NVGo5ZnUzbzJ4MzBiNm1POUNrWHBEb05TNzlCWVlNXzc0Tk05ZkZqNGozYW5hbnhxdUlDblhJNF
9qU2x6WTk1OUVia3NBcUU0MkdaOUtxTXBWNHJCRE1mampfNHpTTEVFZUdhNnh5THVfdm9vR1NyaE5feDc
1RkE4SDVmT3dMRWVqNUVmeG1aX1dscjdudVQ0VlRkTTl2ejJTVTE2RjVKdUxzd2RLc1RZcWVEMkQ1clJp
SWJwTVVubVVXTVpFOTlPVzZnV3d0ZGFZRE0xa04zejgxMG1xcm0ySXM4VUY4enBYOHhlamFIdTdDNVNKR
3EyQSIsImlzc3VlZF9hdCI6MTUzNTgyMDU5NSwidXNlcl9pZCI6IjE3NzI2OTUxNjk0ODYyODUifQ`,
    };

    return this.client.send<any>(pattern, payload);
  }
}
