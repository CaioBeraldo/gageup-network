import request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { UsuariosModule } from '../src/usuarios/usuarios.module';
import { UsuariosService } from '../src/usuarios/usuarios.service';
import { TesteService } from './teste';
import { IUsuario } from '../src/usuarios/interfaces/usuarios.interface';

describe('UsuariosModule (e2e)', () => {
  let app: INestApplication;
  let payload: IUsuario;
  let testService: TesteService<IUsuario>;

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [UsuariosModule],
    })
    .overrideProvider(UsuariosService).useValue(testService)
    .compile();

    app = module.createNestApplication();
    testService = new TesteService<IUsuario>();
    payload = { id: 1, nome: 'Caio Beraldo', email: 'caio.beraldo@fatec.sp.gov.br', facebook_id: 'asdopnqwfpomqwp' };
    await app.init();
  });

  // afterAll(() => { });

  it('Criar usuário (requisição inválida)', () => {
    return request(app.getHttpServer())
      .post('/usuarios')
      .expect(400);
  });

  it('Criar usuário (dados inválidos)', () => {
    return request(app.getHttpServer())
      .post('/usuarios')
      .send({ nome: '', email: '', facebook_id: '' })
      .expect(400);
  });

  it('Criar usuário', () => {
    return request(app.getHttpServer())
      .post('/usuarios')
      .send(payload)
      .expect(201)
      .expect(payload);
  });

  it('Listar todos os usuários.', () => {
    return request(app.getHttpServer())
      .get('/usuarios')
      .expect(200)
      .expect([payload]);
  });

  it(`Buscar usuário ${payload.id}.`, () => {
    return request(app.getHttpServer())
      .get(`/usuarios/${payload.id}`)
      .expect(200)
      .expect(payload);
  });

  it('/PUT /', () => {
    payload.nome = 'ALTERADO';

    return request(app.getHttpServer())
      .get(`/usuarios/${payload.id}`)
      .send(payload)
      .expect(200)
      .expect(payload);
  });

  /*
  it('/DELETE /', () => {
    return request(app.getHttpServer())
      .get('/usuarios')
      .expect(400);
  });
  */
});
