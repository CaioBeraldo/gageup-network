import { Injectable } from '@nestjs/common';

@Injectable()
export class TesteService<T> {
  constructor(private readonly mock_db = []) {}

  async findAll(): Promise<T[]> {
    return this.mock_db;
  }

  async find(id): Promise<T> {
    return this.mock_db.find((mock) => mock.id === id);
  }

  async create(payload): Promise<T> {
    const id = this.mock_db.length + 1;
    payload.id = id;
    this.mock_db.push(payload);
    return this.mock_db.find((mock) => mock.id === id);
  }

  async update(id, payload): Promise<T> {
    const doc = this.mock_db.find((mock) => mock.id === id);
    // Object.keys(doc).forEach(key => {
    //  doc[key] = payload[key];
    // });
    return doc;
  }

  async remove(id) {
    const doc = this.mock_db.splice(id, 1);
    return this.mock_db.find((mock) => mock.id === id);
  }
}