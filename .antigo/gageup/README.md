# gageup

## Description

GageUp Network

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Swagger - documentação

# Exemplos

``` js
/**
 * Fluxo de acesso:
 *  - Login com o facebook (como usuário).
 *  - Listar páginas vinculadas ao usuário (apenas para businessUsers ou systemUsers)
 *  - Buscar informações específicas das páginas.
 *  - 
 */

// BusinessUser / SysUser

// {userID}/assigned_pages
// consultar páginas de um usuário.
// https://developers.facebook.com/docs/marketing-api/businessmanager/systemuser/
// https://business.facebook.com/settings/pages/287739191956252?business_id=252653822256116
FB.api('/102956303996155/assigned_pages', (res) => {
  console.log(res);
});

// {pageID}
// consultar informações de uma página específica.
// https://developers.facebook.com/docs/graph-api/reference/page/
FB.api(
  "/GageUpNetwork", { fields: 'id,about,attire,business,category,category_list,context,cover,description,description_html,display_subtext,engagement,fan_count,current_location,general_info,hours,is_always_open,is_community_page,is_permanently_closed,is_published,link,location,mission,page_token,phone,username,store_number,verification_status,website,whatsapp_number,were_here_count' },
  function (res) {
    console.log(res);
  }
);

// {pageID}/feed
// listar as publicações de uma página.
// https://developers.facebook.com/docs/graph-api/reference/page/
FB.api(
  "/GageUpNetwork/feed", {
    limit: 10 // exibir 10 últimas publicações
  }, function (res) {
    console.log(res);
  }
);

// {pageId}
// obter token de acesso como página.
// https://developers.facebook.com/docs/pages/access-tokens/?locale=pt_BR
FB.api(
  "/GageUpNetwork/", {
    fields: "access_token"
  }, function (res) {
    console.log(res);
  }
);

// {userID}|{pageID}_{objectID}/likes
// visualizar curtidas de uma publicação
FB.api(
  "/287739191956252_287743685289136/likes?access_token=EAADGuKw1TFwBAJpdZAnypr8o1CE3oWhhE0VX556RNLPzGbOhobgXSMdQomZB9ZBTOfoFEHZCSpqiMZBSdogKeAUivZBzUL0FfiuJovNRpq7pwQuaKzlumRuOkZAUCrPzHfGkNgyg9jrEyojDDg4tWAnKtmKIxZBd0ldO08QWa5yEhz7Hc9NT7p8nJdviZAmUqOMpp2QnuIWKgZCwZDZD", function (res) {
    console.log(res);
  }
);

// {userID}|{pageID}_{objectID}/likes
// cutir uma publicação como página.
FB.api(
  "/287739191956252_287743685289136/likes?access_token=EAADGuKw1TFwBAJpdZAnypr8o1CE3oWhhE0VX556RNLPzGbOhobgXSMdQomZB9ZBTOfoFEHZCSpqiMZBSdogKeAUivZBzUL0FfiuJovNRpq7pwQuaKzlumRuOkZAUCrPzHfGkNgyg9jrEyojDDg4tWAnKtmKIxZBd0ldO08QWa5yEhz7Hc9NT7p8nJdviZAmUqOMpp2QnuIWKgZCwZDZD", 'POST', function (res) {
    console.log(res);
  }
);

// {userID}|{pageID}_{objectID}/comments
// comentar em uma publicação como página.
FB.api(
  "/287739191956252_287743685289136/comments?access_token=EAADGuKw1TFwBAJpdZAnypr8o1CE3oWhhE0VX556RNLPzGbOhobgXSMdQomZB9ZBTOfoFEHZCSpqiMZBSdogKeAUivZBzUL0FfiuJovNRpq7pwQuaKzlumRuOkZAUCrPzHfGkNgyg9jrEyojDDg4tWAnKtmKIxZBd0ldO08QWa5yEhz7Hc9NT7p8nJdviZAmUqOMpp2QnuIWKgZCwZDZD", {
    message: 'Hello world'
  }, 'POST', function (res) {
    console.log(res);
  }
);

// https://www.facebook.com/${pageId}/posts/{objectId}
// compartilhar uma publicação
FB.ui({
  method: 'share',
  href: `https://www.facebook.com/${pageId}/posts/287743685289136`,
}, function(response, res1) {
  console.log(response, res1)
});

```