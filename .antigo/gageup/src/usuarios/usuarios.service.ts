import { Injectable, UsePipes, Component, Inject } from '@nestjs/common';
import { IUsuario } from './interfaces/usuarios.interface';
import { InjectModel } from '@nestjs/mongoose';
import { CreateUsuarioPayload } from './payloads/usuarios-create.payload';
import { Model } from 'mongoose';
import { UpdateUsuarioPayload } from './payloads/usuarios-update.payload';

@Injectable()
export class UsuariosService {
  constructor(@InjectModel('Usuario') private readonly usuarios: Model<IUsuario>) {}

  async findAll(): Promise<IUsuario[]> {
    return this.usuarios.find();
  }

  async find(id): Promise<IUsuario> {
    return this.usuarios.findById(id);
  }

  async create(payload: CreateUsuarioPayload): Promise<IUsuario> {
    const usuario = new this.usuarios(payload);
    return usuario.save();
  }

  async update(id, payload: UpdateUsuarioPayload): Promise<IUsuario> {
    const usuario = await this.usuarios.findById(id).exec();
    usuario.update(payload);
    return this.usuarios.findById(id);
  }

  async remove(id) {
    return this.usuarios.findOneAndRemove(id);
  }
}