import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsuariosController } from './usuarios.controller';
import { UsuariosService } from './usuarios.service';
import { UsuariosSchema } from './interfaces/usuarios.schema';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/usuarios'),
    MongooseModule.forFeature([{ name: 'Usuario', schema: UsuariosSchema }]),
  ],
  controllers: [UsuariosController],
  providers: [
    UsuariosService,
  ],
  exports: [UsuariosService],
})
export class UsuariosModule {}
