import { Schema } from 'mongoose';

export const UsuariosSchema = new Schema({
  nome: String,
  email: String,
  userID: String,
  accessToken: String,
  signedRequest: String,
});
