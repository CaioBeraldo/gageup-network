import { Document } from 'mongoose';

export interface IUsuario extends Document {
    readonly nome: string;
    readonly email: string;
    readonly userID: string;
    readonly accessToken: string;
    readonly signedRequest: string;
}
