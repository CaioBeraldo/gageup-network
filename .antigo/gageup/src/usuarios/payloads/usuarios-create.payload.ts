import { IsString, IsEmail, MinLength, MaxLength } from 'class-validator';

export class CreateUsuarioPayload {
    @IsString({ message: 'Nome inválido' })
    @MinLength(5, { message: 'Nome muito curto' })
    @MaxLength(100, { message: 'Nome muito longo' })
    readonly nome: string;

    @IsEmail()
    @MinLength(5, { message: 'E-mail muito curto' })
    @MaxLength(100, { message: 'E-mail muito longo' })
    readonly email: string;

    @IsString()
    @IsString({ message: 'Falha ao logar com facebook (id do usuário)' })
    readonly userID: string;

    @IsString()
    @IsString({ message: 'Falha ao logar com facebook (token de acesso)' })
    readonly accessToken: string;

    @IsString()
    @IsString({ message: 'Falha ao logar com facebook (requisição de acesso)' })
    readonly signedRequest: string;
}
