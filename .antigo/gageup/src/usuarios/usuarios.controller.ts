import { Controller, Get, Post, Put, Delete, Head, Param, Body, UsePipes, ValidationPipe } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { UpdateUsuarioPayload } from './payloads/usuarios-update.payload';
import { CreateUsuarioPayload } from './payloads/usuarios-create.payload';
import { UsuariosService } from './usuarios.service';

@Controller() // 'usuarios'
export class UsuariosController {
  constructor(private readonly usuariosService: UsuariosService) {}

  @MessagePattern({ cmd: 'findAll' })
  findAll() {
    return this.usuariosService.findAll();
  }

  @MessagePattern({ cmd: 'findById' })
  find(@Param('id') id) {
    return this.usuariosService.find(id);
  }

  @UsePipes(new ValidationPipe())
  @MessagePattern({ cmd: 'insert' })
  insert(@Body() createUsuarioPayload: CreateUsuarioPayload) {
    return this.usuariosService.create(createUsuarioPayload);
  }

  @UsePipes(new ValidationPipe())
  @MessagePattern({ cmd: 'update' })
  update(@Param('id') id, @Body() updateUsuarioPayload: UpdateUsuarioPayload) {
    return this.usuariosService.update(id, updateUsuarioPayload);
  }

  @MessagePattern({ cmd: 'remove' })
  remove(@Param('id') id) {
    return this.usuariosService.remove(id);
  }
}