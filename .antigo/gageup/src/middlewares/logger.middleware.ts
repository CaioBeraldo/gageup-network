import { Injectable, NestMiddleware, MiddlewareFunction, HttpStatus, HttpException, UnauthorizedException } from '@nestjs/common';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  resolve(payload): MiddlewareFunction {
    return (req, res, next) => {
      // request logged.
      if (typeof(payload.token) === 'undefined') {
        throw new UnauthorizedException('App token is not valid.');
      }
      else {
        next();
      }
    };
  }
}
