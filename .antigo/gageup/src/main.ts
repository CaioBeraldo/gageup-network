import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/common/enums/transport.enum';
import { UsuariosModule } from 'usuarios/usuarios.module';

async function startUsuariosAPI() {
  const usuariosApp = await NestFactory.createMicroservice(UsuariosModule, {
    transport: Transport.TCP,
    options: {
      host: 'localhost',
      port: 3002,
    },
  });

  usuariosApp.listen(() => {});
}

startUsuariosAPI();
