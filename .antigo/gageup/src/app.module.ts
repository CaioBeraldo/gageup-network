import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { UsuariosModule } from 'usuarios/usuarios.module';
import { LoggerMiddleware } from 'middlewares/logger.middleware';

@Module({
  imports: [UsuariosModule],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    /*
    consumer
      .apply(
          LoggerMiddleware,
          // InjectionsMiddleware,
          // TokenMiddleware,
          // AuthenticationMiddleware,
          // AuthorizationMiddleware,
      )
      .with({
        token: 'appToken',
      })
      .forRoutes('usuarios');
    */
  }
}
