import { Injectable, PipeTransform, ArgumentMetadata, BadRequestException } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
  async transform(value, { metatype }: ArgumentMetadata) {
    if (!this.needsValidation(metatype)) {
      return value;
    }

    const erros = await validate(plainToClass(metatype, value));
    if (erros.length > 0) {
      throw new BadRequestException('Validation failed');
    }

    return value;
  }

  private needsValidation(metatype): boolean {
    const types = [String, Boolean, Number, Array, Object];
    return !metatype || !types.find((type) => metatype === type);
  }
}