const gulp = require('gulp')
const uglify = require('gulp-uglify')
const babel = require('gulp-babel')
const concat = require('gulp-concat')
const sourcemaps = require('gulp-sourcemaps')
const plumberNotifier = require('gulp-plumber-notifier');
const copy = require('gulp-copy')

gulp.task('uglifyShared', () => {
  // compilar e minificar.
  gulp.src('./src/dist/**/*.js')
    .pipe(plumberNotifier())
    .pipe(babel({ presets: ['@babel/env'] }))
    .pipe(sourcemaps.init())
    .pipe(concat('index.js'))
    .pipe(uglify())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./dist/'))

  // copiar outros arquivos.
  gulp.src('./package.json')
    .pipe(copy('./dist/'))
})

gulp.task('default', ['uglifyShared'])
