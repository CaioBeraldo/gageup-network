
# Inicialização

``` js
npm install class-transformer class-validator mongoose
```

``` json
{
  "dependencies": {
    "class-transformer": "^0.1.9",
    "class-validator": "^0.9.1",
    "mongoose": "^5.2.15"
  }
}
```
