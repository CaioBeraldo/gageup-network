import { Document } from 'mongoose';

export interface IUsuario extends Document {
  readonly nome: { type: string };
  readonly email: { type: string };
  // readonly password: { type: string };
  // readonly username: { type: string };
  readonly userID: { type: string };
  readonly accessToken: { type: string };
  readonly signedRequest: { type: string };
}
