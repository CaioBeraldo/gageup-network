import { Document } from 'mongoose';
import { IUsuario } from './usuarios.interface';

export interface IAuthToken extends Document {
  readonly usuario: IUsuario,
  readonly usuario_id: { type: String },
  readonly accessToken: { type: String },
  readonly accessTokenExpiresOn: Date,
}
