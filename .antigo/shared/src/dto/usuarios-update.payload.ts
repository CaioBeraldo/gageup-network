import { IsString, IsEmail, MinLength, MaxLength } from 'class-validator';

export class UpdateUsuarioPayload {
    @IsString({ message: 'Nome inválido' })
    @MinLength(5, { message: 'Nome muito curto' })
    @MaxLength(100, { message: 'Nome muito longo' })
    readonly nome: string;

    @IsEmail()
    @MinLength(5, { message: 'E-mail muito curto' })
    @MaxLength(100, { message: 'E-mail muito longo' })
    readonly email: string;
}
