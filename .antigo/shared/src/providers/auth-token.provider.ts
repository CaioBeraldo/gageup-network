import { Connection } from 'mongoose';
import { OAuthToken } from '../schemas/auth-token.schema';
import { Constants } from '../contants';

export const authTokenProvider = [
  {
    provide: Constants.DATABASE.AUTH_TOKEN.TOKEN,
    useFactory: (connection: Connection) => connection.model(Constants.DATABASE.AUTH_TOKEN.MODEL, OAuthToken),
    inject: [Constants.DATABASE.TOKEN],
  },
];
