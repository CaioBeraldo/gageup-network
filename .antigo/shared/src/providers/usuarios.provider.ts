import { Connection } from 'mongoose';
import { UsuariosSchema } from '../schemas/usuarios.schema';
import { Constants } from '../contants';

export const usuariosProvider = [
  {
    provide: Constants.DATABASE.USUARIO.TOKEN,
    useFactory: (connection: Connection) => connection.model(Constants.DATABASE.USUARIO.MODEL, UsuariosSchema),
    inject: [Constants.DATABASE.TOKEN],
  },
];
