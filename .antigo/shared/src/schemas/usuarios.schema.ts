import { Schema } from 'mongoose';
// import * as passportLocalMongoose from 'passport-local-mongoose';
// import * as passportLocalMongoose from 'passport-local-mongoose';

export const UsuariosSchema = new Schema({
  nome: { type: String },
  email: { type: String },
  // password: { type: String },
  // username: { type: String },
  userID: { type: String },
  accessToken:  { type: String },
  signedRequest:  { type: String },
});

// UsuariosSchema.plugin(passportLocalMongoose);
