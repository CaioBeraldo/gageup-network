import { Schema } from 'mongoose';

export const OAuthToken = new Schema({
  usuario : { type: Object },
  usuario_id: { type: String },
  accessToken: { type: String },
  accessTokenExpiresOn: { type: Date },
});
