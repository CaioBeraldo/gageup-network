#!/bin/bash
# chmod +x release.sh

# remover arquivos dinamicamente da pasta /dist
# rimraf dist 

# remover arquivos dinamicamente da pasta /dist sem remover a pasta
rm -r src/dist/*.js
rm -r src/dist/*.ts
rm -r src/dist/*.map
rm -r src/dist/*.html
rm -r src/dist/**/*.js
rm -r src/dist/**/*.ts
rm -r src/dist/**/*.map
rm -r src/dist/**/*.html
# limpar diretórios vazios
rm -d src/dist/*

# compilar arquivos .ts em arquivos .js e .map na pasta /dist
tsc

# cp ./package*.json ./src/dist
# cp -r ./src/public/ ./dist/public/

# npm run gulp

# cd src/dist
# npm i
# git add .
# git commit
# git push
# cd ../..
