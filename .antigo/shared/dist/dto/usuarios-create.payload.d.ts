export declare class CreateUsuarioPayload {
    readonly nome: string;
    readonly email: string;
    readonly userID: string;
    readonly accessToken: string;
    readonly signedRequest: string;
}
