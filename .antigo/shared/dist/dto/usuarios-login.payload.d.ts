export declare class LoginUsuarioPayload {
    readonly email: {
        type: string;
    };
    readonly password: {
        type: string;
    };
}
