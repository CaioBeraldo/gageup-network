"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Constants = {
    DATABASE: {
        USUARIO: {
            TOKEN: 'VXN1YXJpb01vZGVsVG9rZW4=',
            MODEL: 'Usuario',
        },
        AUTH_TOKEN: {
            TOKEN: 'T0F1dGhUb2tlblRva2Vu',
            MODEL: 'OAuthToken',
        },
        TOKEN: 'RGJDb25uZWN0aW9uVG9rZW4=',
    },
    MICROSERVICE: {
        AUTH_TOKEN: {
            SIGNIN: { cmd: 'signin', controller: 'auth' },
        },
        USUARIO: {
            CREATE: { cmd: 'create', controller: 'usuarios' },
        },
    }
};
//# sourceMappingURL=contants.js.map