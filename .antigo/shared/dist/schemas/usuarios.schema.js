"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.UsuariosSchema = new mongoose_1.Schema({
    nome: { type: String },
    email: { type: String },
    userID: { type: String },
    accessToken: { type: String },
    signedRequest: { type: String },
});
//# sourceMappingURL=usuarios.schema.js.map