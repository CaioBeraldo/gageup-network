"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.OAuthToken = new mongoose_1.Schema({
    usuario: { type: Object },
    usuario_id: { type: String },
    accessToken: { type: String },
    accessTokenExpiresOn: { type: Date },
});
//# sourceMappingURL=auth-token.schema.js.map