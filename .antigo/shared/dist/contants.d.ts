export declare const Constants: {
    DATABASE: {
        USUARIO: {
            TOKEN: string;
            MODEL: string;
        };
        AUTH_TOKEN: {
            TOKEN: string;
            MODEL: string;
        };
        TOKEN: string;
    };
    MICROSERVICE: {
        AUTH_TOKEN: {
            SIGNIN: {
                cmd: string;
                controller: string;
            };
        };
        USUARIO: {
            CREATE: {
                cmd: string;
                controller: string;
            };
        };
    };
};
