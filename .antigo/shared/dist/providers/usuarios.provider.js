"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const usuarios_schema_1 = require("../schemas/usuarios.schema");
const contants_1 = require("../contants");
exports.usuariosProvider = [
    {
        provide: contants_1.Constants.DATABASE.USUARIO.TOKEN,
        useFactory: (connection) => connection.model(contants_1.Constants.DATABASE.USUARIO.MODEL, usuarios_schema_1.UsuariosSchema),
        inject: [contants_1.Constants.DATABASE.TOKEN],
    },
];
//# sourceMappingURL=usuarios.provider.js.map