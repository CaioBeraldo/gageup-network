"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const auth_token_schema_1 = require("../schemas/auth-token.schema");
const contants_1 = require("../contants");
exports.authTokenProvider = [
    {
        provide: contants_1.Constants.DATABASE.AUTH_TOKEN.TOKEN,
        useFactory: (connection) => connection.model(contants_1.Constants.DATABASE.AUTH_TOKEN.MODEL, auth_token_schema_1.OAuthToken),
        inject: [contants_1.Constants.DATABASE.TOKEN],
    },
];
//# sourceMappingURL=auth-token.provider.js.map