export declare const authTokenProvider: {
    provide: string;
    useFactory: (connection: any) => any;
    inject: string[];
}[];
