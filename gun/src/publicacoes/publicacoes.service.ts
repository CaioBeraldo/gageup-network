import { Injectable, Inject, Logger } from '@nestjs/common';
import { Constants } from '../contants';
import { Model } from 'mongoose';
import { IPublicacao } from './publicacoes.interface';
import { EngajarPublicacaoPayload } from './dto/publicacoes-engajar';

@Injectable()
export class PublicacoesService {
  constructor(@Inject(Constants.DATABASE.PUBLICACOES.TOKEN) private readonly publicacoes: Model<IPublicacao>) {}

  findAll() {
    return this.publicacoes.find();
  }

  async engajarPublicacao(payload: EngajarPublicacaoPayload) {
    let publicacao = this.publicacoes.findOne(payload.publicacao).lean();

    /*
    if (publicacao) {
      // atualiza os pontos da publicação
      return this.publicacoes.updateOne(
        { _id: payload.publicacao.usuario_id },
        { $set: { pontos: publicacao.pontos + payload.pontos }});
    }
    */

    publicacao = new this.publicacoes(payload);
    return publicacao.save();
  }
}