import { Connection } from 'mongoose';
import { Constants } from '../contants';
import { PublicacoesSchema } from './publicacoes.schema';

export const publicacoesProvider = [
  {
    provide: Constants.DATABASE.PUBLICACOES.TOKEN,
    useFactory: (connection: Connection) =>
      connection.model(Constants.DATABASE.PUBLICACOES.MODEL, PublicacoesSchema),
    inject: [Constants.DATABASE.TOKEN],
  },
];
