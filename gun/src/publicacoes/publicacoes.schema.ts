import { Schema } from 'mongoose';

export const PublicacoesSchema = new Schema({
  objeto_id: { type: String },
  publicacao: { type: Object },
  pontos: { type: Number },
});
