import { Get, Controller, Body, Post, UseGuards, Logger, UsePipes, ValidationPipe, Res, Req } from '@nestjs/common';
import { PublicacoesService } from './publicacoes.service';
import { EngajarPublicacaoPayload } from './dto/publicacoes-engajar';
import { AuthGuard } from '@nestjs/passport';

@Controller('publicacoes')
@UseGuards(AuthGuard('jwt'))
export class PublicacoesController {
  constructor(private readonly publicacoesService: PublicacoesService) {}

  @Get()
  async findAll() {
    return await this.publicacoesService.findAll();
  }

  @Post()
  // @UsePipes(new ValidationPipe())
  async post(@Req() req, @Body() payload: EngajarPublicacaoPayload) {
    Logger.log(req.user);
    return payload; // await this.publicacoesService.engajarPublicacao(payload);
  }

}
