import { Module } from '@nestjs/common';
import { PublicacoesController } from 'publicacoes/publicacoes.controller';
import { publicacoesProvider } from './publicacoes.provider';
import { PublicacoesService } from './publicacoes.service';
import { DatabaseModule } from 'database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [PublicacoesController],
  providers: [PublicacoesService, ...publicacoesProvider],
  exports: [PublicacoesService, ...publicacoesProvider],
})
export class PublicacoesModule {}
