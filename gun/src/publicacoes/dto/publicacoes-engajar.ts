import { IsMongoId, IsNumber, IsInt, IsJSON, Min } from 'class-validator';
import { IPublicacao } from '../publicacoes.interface';

export class EngajarPublicacaoPayload {
  @IsMongoId({ message: 'Invalid payload.' })
  readonly objeto_id: string;

  @Min(1)
  @IsInt()
  readonly pontos: number;

  @IsJSON()
  readonly publicacao: IPublicacao;
}
