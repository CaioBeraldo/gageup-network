import { Document } from 'mongoose';

export interface IPublicacao extends Document {
  readonly usuario_id: { type: string };
  readonly nome: { type: string };
  readonly email: { type: string };
  readonly passwordHash: { type: string };
  readonly userID: { type: string };
  readonly paginas: Array<object>;
}
