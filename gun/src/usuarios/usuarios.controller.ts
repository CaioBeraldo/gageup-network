import { Get, Controller, Res, Param, HttpStatus, Post, Body, ValidationPipe, UsePipes, UseGuards, Logger, Req } from '@nestjs/common';
import { UsuariosService } from './usuarios.service';
import { AuthGuard } from '@nestjs/passport';

@Controller('usuarios')
@UseGuards(AuthGuard('jwt'))
export class UsuariosController {
  constructor(private readonly usuariosService: UsuariosService) {}

  /*
  @Get()
  async findAll(@Res() res) {
    const data = await this.usuariosService.findAll();
    res.status(HttpStatus.FOUND).send(data);
  }
  */

  @Get()
  validateToken(@Req() req, @Res() res) {
    res.status(HttpStatus.OK).send(req.user);
  }

  @Get(':id')
  async find(@Res() res, @Param('id') id) {
    const data = await this.usuariosService.find(id);
    res.status(HttpStatus.FOUND).send(data);
  }

  @Get('/delete')
  async teste() {
    const usuarios = await this.usuariosService.findAll();
    usuarios.forEach(async (usuario) => {
      await this.usuariosService.remove(usuario._id);
    });
  }

  /*
  @Post()
  @UsePipes(new ValidationPipe())
  create(@Res() res, @Body() createUsuarioPayload: CreateUsuarioPayload) {
    const data = this.usuariosService.create(createUsuarioPayload);
    res.status(HttpStatus.CREATED).send({ ...data });
  }
  */

  /*
  @Get()
  async findAll(@Res() res) {
    const pattern = { cmd: 'findAll' };
    const payload = { };
    const data = await this.client.send<Array<any[]>>(pattern, payload).toPromise();

    res.status(HttpStatus.FOUND).json(data);
  }

  @Get(':id')
  async find(@Res() res, @Param('id') id) {
    const pattern = { cmd: 'findById' };
    const payload = id;
    const data = await this.client.send<Array<any[]>>(pattern, payload).toPromise();

    res.status(HttpStatus.FOUND).json(data);
  }

  */
}
