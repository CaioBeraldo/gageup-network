import { IsString, IsEmail, MinLength, MaxLength, Contains, Matches } from 'class-validator';

export class CreatePasswordUsuarioPayload {
  @IsEmail()
  readonly email: string;

  @IsString({ message: 'Senha inválida' })
  @MinLength(8, { message: 'Senha muito curta' })
  @MaxLength(30, { message: 'Senha muito longa' })
  @Matches(/^(?=(.*[A-Z]){1})(?=.*[!@#$&*])(?=(.*[0-9]){1})(?=(.*[a-z]){1}).{8,}$/, {
    message: 'Senha deve conter pelo menos 1 letra maiúscula, 1 letra minúscula, 1 número e deve conter no mínimo 8 caracteres.'
  })
  readonly password: string;

  @IsString()
  @IsString({ message: 'Falha ao logar com facebook (id do usuário)' })
  readonly userID: string;
}
