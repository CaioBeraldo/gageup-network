import { IsString, IsEmail, MinLength, MaxLength, Contains, Matches } from 'class-validator';

export class CreateUsuarioPayload {
    @IsString({ message: 'Nome inválido' })
    @MinLength(5, { message: 'Nome muito curto' })
    @MaxLength(100, { message: 'Nome muito longo' })
    readonly nome: string;

    @IsEmail()
    @MinLength(5, { message: 'E-mail muito curto' })
    @MaxLength(100, { message: 'E-mail muito longo' })
    readonly email: string;

    // @IsString({ message: 'Senha inválida' })
    // @MinLength(8, { message: 'Senha muito curta' })
    // @MaxLength(30, { message: 'Senha muito longa' })
    /**
     * 1 - pelo menos {N} letras maiúscula.
     * 2 - pelo menos {N} letras minúscula.
     * 3 - pelo menos {N} números.
     * 4 - pelo menos {N} caracteres.
     */
    // @Matches(/^(?=(.*[A-Z]){1})(?=.*[!@#$&*])(?=(.*[0-9]){1})(?=(.*[a-z]){1}).{8,}$/)
    // readonly password: string;

    @IsString()
    @IsString({ message: 'Falha ao logar com facebook (id do usuário)' })
    readonly userID: string;
}
