import { IsString, IsNotEmpty } from 'class-validator';

export class LoginUsuarioPayload {
  @IsString({ message: 'E-mail inválido.' })
  @IsNotEmpty({ message: 'E-mail deve ser preenchido.' })
  readonly email: { type: string };

  @IsString({ message: 'Senha inválida.' })
  @IsNotEmpty({ message: 'Senha deve ser preenchida.' })
  readonly password: { type: string };
}
