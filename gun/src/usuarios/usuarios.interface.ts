import { Document } from 'mongoose';

export interface IUsuario extends Document {
  readonly _id: { type: string };
  readonly nome: { type: string };
  readonly email: { type: string };
  readonly passwordHash: { type: string };
  readonly userID: { type: string };
  readonly paginas: Array<object>;
}

export interface IHashCompare {
  readonly password: { type: string };
  readonly passwordHash: { type: string };
}