import { Schema } from 'mongoose';
// import * as passportLocalMongoose from 'passport-local-mongoose';

export const UsuariosSchema = new Schema({
  nome: { type: String },
  email: { type: String },
  passwordHash: { type: String },
  userID: { type: String },
  paginas: { type: Array, default: [] },
});

// UsuariosSchema.plugin(passportLocalMongoose);
