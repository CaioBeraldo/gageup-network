import { Injectable, Inject, UseGuards, Logger } from '@nestjs/common';
import { IUsuario } from './usuarios.interface';
import { CreateUsuarioPayload } from './dto/usuarios-create.payload';
import { UpdateUsuarioPayload } from './dto/usuarios-update.payload';
import { LoginUsuarioPayload } from './dto/usuarios-login.payload';
import { CreatePasswordUsuarioPayload } from './dto/usuarios-create-password.payload';
import { IHashCompare } from './usuarios.interface';
import { Constants } from '../contants';
import { Model } from 'mongoose';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsuariosService {
  private readonly saltRounds = 10;
  constructor(@Inject(Constants.DATABASE.USUARIO.TOKEN) private readonly usuariosService: Model<IUsuario>) {}

  async create(payload: CreateUsuarioPayload): Promise<IUsuario> {
    const usuario = new this.usuariosService(payload);
    return await usuario.save();
  }

  getUserByLogin(payload: LoginUsuarioPayload): Promise<IUsuario> {
    const email = payload.email;
    return this.usuariosService.findOne({ email }).lean();
  }

  getUserByEmail(email) {
    return this.usuariosService.findOne({ email }).lean();
  }

  getUserByFacebookId(userID): Promise<IUsuario> {
    return this.usuariosService.findOne({ userID }).lean();
  }

  addPage(usuario: IUsuario, page: object): Promise<any> {
    const paginas = usuario.paginas ? usuario.paginas.concat(page) : [page];
    return this.usuariosService.updateOne({ _id: usuario._id }, { $set: { paginas }});
  }

  findPage(page_token: string): Promise<any> {
    return this.usuariosService.find({ paginas: { $elemMatch: { page_token } } }).lean();
  }

  find(id): Promise<IUsuario> {
    return this.usuariosService.findById(id);
  }

  getHash(password: string|undefined): string {
    const salt = bcrypt.genSaltSync();
    return bcrypt.hashSync(password, salt);
  }

  compareHash(hashCompare: IHashCompare): boolean {
    return bcrypt.compareSync(hashCompare.password, hashCompare.passwordHash);
  }

  async createPassword(payload: CreatePasswordUsuarioPayload, passwordHash: string): Promise<IUsuario> {
    await this.usuariosService.updateOne({ email: payload.email }, { $set: { passwordHash }});
    return this.getUserByEmail(payload.email);
  }

  async validateUser(payload: LoginUsuarioPayload): Promise<any> {
    const usuario = await this.getUserByLogin(payload);
    if (usuario) return true;
    return false;
  }

  /*
  async getAccessToken(bearerToken) {
    return this.usuarios.findOne({ accessToken: bearerToken }).lean();
  }

  async getClient(clientId, clientSecret) {
    return this.authClient.findOne({ clientId, clientSecret }).lean();
  }

  async getRefreshToken(refreshToken) {
    return this.authToken.findOne({ refreshToken }).lean();
  }

  async getUser(username, password) {
    return this.usuarios.findOne({ username, password }).lean();
  }

  async saveToken(token, client, user) {
    const accessToken = new this.authToken({
      accessToken: token.accessToken,
      accessTokenExpiresOn: token.accessTokenExpiresOn,
      client,
      clientId: client.clientId,
      refreshToken: token.refreshToken,
      refreshTokenExpiresOn: token.refreshTokenExpiresOn,
      user,
      userId: user._id,
    });

    // Can't just chain `lean()` to `save()` as we did with `findOne()` elsewhere. Instead we use `Promise` to resolve the data.
    return new Promise((resolve, reject) => {
      accessToken.save((err, data) => {
        if (err) reject(err);
        else resolve(data);
      });
    }).then((saveResult: IAuthToken) => {
      // `saveResult` is mongoose wrapper object, not doc itself. Calling `toJSON()` returns the doc.
      // saveResult = saveResult && typeof saveResult === 'object' ? Object(saveResult).toJSON() : saveResult;

      // Unsure what else points to `saveResult` in oauth2-server, making copy to be safe
      // const data = new Object();
      // for (const prop in saveResult) data[prop] = saveResult[prop];

      // /oauth-server/lib/models/token-model.js complains if missing `client` and `user`. Creating missing properties.
      // saveResult.client = data.clientId;
      // saveResult.user = data.userId;

      return saveResult;
    });
  }
*/
  /**
   * Outras funções.
   */

  async findAll(): Promise<IUsuario[]> {
    return this.usuariosService.find();
  }

  async update(id, payload: UpdateUsuarioPayload): Promise<IUsuario> {
    let usuario = await this.usuariosService.findById(id);
    usuario = Object.assign(usuario, payload);
    return usuario.save(); // this.usuariosService.findById(id);
  }

  async remove(id) {
    return this.usuariosService.findOneAndDelete(id);
  }

}