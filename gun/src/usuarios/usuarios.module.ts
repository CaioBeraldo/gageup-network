import { Module } from '@nestjs/common';
import { UsuariosController } from './usuarios.controller';
import { UsuariosService } from './usuarios.service';
import { usuariosProvider } from './usuarios.provider';
import { DatabaseModule } from '../database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [UsuariosController],
  providers: [UsuariosService, ...usuariosProvider],
  exports: [UsuariosService, ...usuariosProvider],
})
export class UsuariosModule {}
