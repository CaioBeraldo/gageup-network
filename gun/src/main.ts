import { NestFactory, FastifyAdapter } from '@nestjs/core';
import { AppModule } from './app.module';
import { join } from 'path';
import { Constants } from 'contants';
import * as cookieParser from 'cookie-parser';
import * as expressSession from 'express-session';
import * as fastify from 'fastify';
import * as fastifyCompress from 'fastify-compress';
import * as fastifyRateLimit from 'fastify-rate-limit';
import * as fs from 'fs';
import { Logger } from '@nestjs/common';

async function startApp() {
  const port = process.env.PORT || 3001;

  // self-trusted tsl certified.
  // utilizado para teste das APIs do facebook, só aceita requisições https.
  // apenas em ambiente de desenvolvimento.
  const httpsOptions = {
    key: fs.readFileSync('./cert/server.key'),
    cert: fs.readFileSync('./cert/server.crt'),
    requestCert: false,
    rejectUnauthorized: false,
  };

  const fastifyAdapter = new FastifyAdapter({ https: httpsOptions, /* http2: true, */ });
  const app = await NestFactory.create(AppModule, fastifyAdapter);

  // Server configuration
  // app.use(json());
  // app.use(urlencoded({ extended: false }));
  app.use(cookieParser());
  app.use(expressSession({
    secret: process.env.SECRET_OR_PRIVATE_KEY || Constants.APPLICATION.SECRET_OR_PRIVATE_KEY,
    resave: false,
    saveUninitialized: false,
    cookie: { maxAge: 60000 },
  }));
  app.useStaticAssets({
    root: join(__dirname, 'public'),
  });

  // Security Configurations
  // app.use(helmet());
  // app.use(csurf());
  app.enableCors();

  // Performance Configurations.
  fastify().register(fastifyCompress, {
    global: false,
  });
  fastify().register(fastifyRateLimit, {
    max: 1000, // default 1000
    timeWindow: 15 * 60 * 1000, // default 1000 * 60
    cache: 10000, // default 5000
    whitelist: ['127.0.0.1'], // default []
    skipOnError: true, // default false
  });

  app.listen(port, () => {
    Logger.log(`Server listenning on port https://localhost:${port}`)
  });

}

startApp();
