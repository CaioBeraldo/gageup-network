import * as mongoose from 'mongoose';
import { Constants } from '../contants';

const database_connection = 'mongodb://localhost/gageupnetwork';

// MongooseModule.forRoot('mongodb://localhost/usuarios', { useNewUrlParser: true }),
// MongooseModule.forFeature([{ name: 'Usuario', schema: UsuariosSchema }]),
export const databaseProviders = [
  {
    provide: Constants.DATABASE.TOKEN,
    useFactory: async (): Promise<typeof mongoose> =>
      await mongoose.connect(process.env.DATABASE_CONNECTION || database_connection, { useNewUrlParser: true }),
  },
];
