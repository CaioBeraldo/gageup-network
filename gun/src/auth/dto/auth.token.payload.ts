
export class TokenPayload {
  readonly _id: { type: string };
  readonly email: { type: string };
}
