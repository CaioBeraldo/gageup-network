import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { Constants } from '../contants';

export const JwtRegister = JwtModule.register({
  secretOrPrivateKey: process.env.SECRET_OR_PRIVATE_KEY || Constants.APPLICATION.SECRET_OR_PRIVATE_KEY,
  signOptions: {
    expiresIn: 60 * 60 * 5, // 60 segundos (1 minuto) * 60 minutos (1 hora) * 24 horas (1 dia).
  },
});

export const PmRegister = PassportModule.register({
  property: 'user',
  defaultStrategy: 'jwt',
  session: true,
});
