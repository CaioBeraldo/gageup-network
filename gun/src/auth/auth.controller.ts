import { Controller, Body, UsePipes, ValidationPipe, Res, HttpStatus, Post, Put, Get, Logger, HttpService, Param, Req } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginUsuarioPayload } from '../usuarios/dto/usuarios-login.payload';
import { UsuariosService } from '../usuarios/usuarios.service';
import { IHashCompare, IUsuario } from '../usuarios/usuarios.interface';
import { CreateUsuarioPayload } from '../usuarios/dto/usuarios-create.payload';
import { CreatePasswordUsuarioPayload } from 'usuarios/dto/usuarios-create-password.payload';
import { TokenPayload } from './dto/auth.token.payload';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly usuariosService: UsuariosService,
    private readonly authService: AuthService,
  ) {}

  @Post('/signin')
  @UsePipes(new ValidationPipe())
  async signin(@Res() res, @Body() payload: LoginUsuarioPayload) {
    const usuario = await this.usuariosService.getUserByLogin(payload);

    if (!usuario) {
      res.status(HttpStatus.BAD_REQUEST).send({ error: 'Usuário ou senha inválido!' });
      return;
    }

    if (!usuario.passwordHash) {
      res.status(HttpStatus.UNAUTHORIZED).send({ error: 'Senha não cadastrada para o usuário!' });
      return;
    }

    const hashCompare: IHashCompare = { password: payload.password, passwordHash: usuario.passwordHash };
    const result = this.usuariosService.compareHash(hashCompare);

    if (!result) {
      res.status(HttpStatus.UNAUTHORIZED).send({ error: 'Usuário ou senha inválidos.' });
      return;
    }

    const tokenPayload: TokenPayload = { _id: usuario._id, email: usuario.email };
    const token = await this.authService.createToken(tokenPayload);

    res.status(HttpStatus.OK).send({
      ...usuario,
      passwordHash: undefined,
      token,
    });
  }

  @Post()
  @UsePipes(new ValidationPipe())
  async register(@Res() res, @Body() payload: CreateUsuarioPayload) {
    let usuario = await this.usuariosService.getUserByEmail(payload.email);

    if (usuario) {
      res.status(HttpStatus.FOUND).send({ error: 'Usuário já cadastrado.' });
      return;
    }

    usuario = await this.usuariosService.create(payload);

    res.status(HttpStatus.OK).send(usuario);
  }

  @Put()
  @UsePipes(new ValidationPipe())
  async createPassword(@Res() res, @Body() payload: CreatePasswordUsuarioPayload) {
    let usuario = await this.usuariosService.getUserByEmail(payload.email);

    if (!usuario) {
      res.status(HttpStatus.BAD_REQUEST).send({ error: 'Usuário não cadastrado.' });
      return;
    }
    if (usuario.passwordHash) {
      res.status(HttpStatus.FOUND).send({ error: 'Senha já cadastrada, para alterar utilize a opção de alteração de senha.' });
      return;
    }

    const passwordHash = this.usuariosService.getHash(payload.password);
    usuario = await this.usuariosService.createPassword(payload, passwordHash);

    if (usuario) {
      // oculta hash no retorno da requisição.
      usuario.passwordHash = undefined;
    }

    res.status(HttpStatus.OK).send(usuario);
  }

  @Get(':id')
  @UsePipes(new ValidationPipe())
  async validateUser(@Res() res, @Param('id') id) {
    const usuario = await this.usuariosService.getUserByFacebookId(id);

    if (!usuario) {
      res.status(HttpStatus.NOT_FOUND).send({ existe: false });
      return;
    }

    res.status(HttpStatus.FOUND).send({ existe: true, email: usuario.email });
  }

}
