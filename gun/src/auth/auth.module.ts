import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsuariosModule } from '../usuarios/usuarios.module';
import { AuthController } from './auth.controller';
import { JwtRegister, PmRegister } from './auth.imports';
import { JwtStrategy } from './jwt.strategy';

@Module({
  imports: [PmRegister, JwtRegister, UsuariosModule],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy],
  exports: [],
})
export class AuthModule { }