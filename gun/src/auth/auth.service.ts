import { Injectable, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { TokenPayload } from './dto/auth.token.payload';
import { UsuariosService } from 'usuarios/usuarios.service';
import { IUsuario } from 'usuarios/usuarios.interface';
import { PaginaPayload } from 'paginas/dto/paginas.payload';

@Injectable()
export class AuthService {
  constructor(
    private readonly usuariosService: UsuariosService,
    private readonly jwt: JwtService,
  ) {}

  async createToken(payload: TokenPayload): Promise<string> {
    const token = this.jwt.sign({ ...payload });
    return token;
  }

  async validateUser(payload: TokenPayload) {
    const usuario: IUsuario = await this.usuariosService.getUserByEmail(payload.email);

    if (!usuario) {
      return;
    }

    return {
      ...usuario,
      paginas: usuario.paginas.map((pagina: PaginaPayload) => ({ id: pagina.id })),
      passwordHash: undefined,
    };
  }

}