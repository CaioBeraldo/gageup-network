import { Injectable, NestMiddleware, MiddlewareFunction, UnauthorizedException } from '@nestjs/common';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  resolve(payload): MiddlewareFunction {
    return (req, res, next) => {
      if (typeof(payload.token) === 'undefined') {
        throw new UnauthorizedException('App token is not valid.');
      }
      else {
        next();
      }
    };
  }
}
