import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { PaginasController } from './paginas.controller';
import { UsuariosModule } from 'usuarios/usuarios.module';

@Module({
  imports: [DatabaseModule, UsuariosModule],
  controllers: [PaginasController],
  providers: [],
  exports: [],
})
export class PaginasModule {}
