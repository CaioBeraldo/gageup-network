import { IsString } from 'class-validator';
import { PaginaPayload } from './paginas.payload';

export class AddPaginasPayload {
  @IsString()
  @IsString({ message: 'Usuário não está conectado ao Facebook.' })
  readonly userID: string;

  readonly pagina: PaginaPayload;
}
