
export class PaginaPayload {
  readonly id: string;
  readonly page_token: string;
}
