import { Get, Controller, Res, Param, HttpStatus, Post, Body, ValidationPipe, UsePipes, UseGuards, Logger } from '@nestjs/common';
import { UsuariosService } from '../usuarios/usuarios.service';
import { AuthGuard } from '@nestjs/passport';
import { AddPaginasPayload } from './dto/paginas-add.payload';

@Controller('paginas')
@UseGuards(AuthGuard('jwt'))
export class PaginasController {
  constructor(private readonly usuariosService: UsuariosService) {}

  @Post()
  async addPage(@Res() res, @Body() payload: AddPaginasPayload) {
    const usuario = await this.usuariosService.getUserByFacebookId(payload.userID);

    if (!usuario) {
      res.status(HttpStatus.NOT_FOUND).send();
      return;
    }

    const usuario_pagina = await this.usuariosService.findPage(payload.pagina.page_token);
    Logger.log(usuario_pagina);
    if (usuario_pagina.length === 0) {
      const updated = await this.usuariosService.addPage(usuario, payload.pagina);

      if (updated.ok <= 0) {
        res.status(HttpStatus.NOT_MODIFIED).send({});
        return;
      }

      res.status(HttpStatus.ACCEPTED).send(updated);
      return;
    }

    res.status(HttpStatus.NOT_MODIFIED).send({});
  }

}

/*
  {
    "id":"287739191956252",
    "business":{
      "id":"252653822256116",
      "name":"GageUp Network"
    },
    "category":"Internet Marketing Service",
    "category_list":[
      {"id":"1706730532910578","name":"Internet Marketing Service"}
    ],
    "cover": {
      "cover_id":"296887567708081",
      "offset_x":49,
      "offset_y":50,
      "source":"https://scontent.xx.fbcdn.net/v/t1.0-9/s720x720
        /42691245_296887571041414_4857604459838046208_n.jpg?_nc_cat
        =101&oh=c5ec27deaf526bf450f4319c9215520c&oe=5C1B89AE",
      "id":"296887567708081"
    },
    "display_subtext": "Rua Gonçalves Ledo・São Paulo, Brazil",
    "engagement": {
      "count":1,
      "social_sentence":"You like this."
    },
    "fan_count":1,
    "is_always_open":false,
    "is_community_page":false,
    "is_permanently_closed":false,
    "is_published":true,
    "link":"https://www.facebook.com/GageUpNetwork/",
    "location":{
      "city":"São Paulo",
      "country":"Brazil",
      "latitude":-23.59699,
      "longitude":-46.6016199,
      "state":"SP",
      "street":"Rua Gonçalves Ledo",
      "zip":"17511402"
    },
    "page_token":"GageUpNetwork",
    "username":"GageUpNetwork",
    "verification_status":"not_verified",
    "were_here_count":0
  }
*/