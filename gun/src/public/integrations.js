const CONSTANTS = {
  APP_ID: 'MjE4NDk2NDY1NDU2MjIw',
  USER_PERMISSIONS: 'dXNlcl9saWtlcyx1c2VyX3Bvc3RzLGVtYWlsLHB1Ymxpc2hfdG9fZ3JvdXBzLHB1Ymxpc2hfcGFnZXMsbWFuYWdlX3BhZ2Vz',
  FB_API_VERSION: 'djMuMQ==',
  get: (key) => {
    return atob(CONSTANTS[key])
  },
}

class FacebookSDK {
    constructor() {
        this.init = () => {
            window.fbAsyncInit = () => {
                let appId = CONSTANTS.get('APP_ID');
                let version = CONSTANTS.get('FB_API_VERSION');
                FB.init({ appId: appId, cookie: true, xfbml: true, version: version });
                FB.AppEvents.logPageView();
            };
        };
        this.login = () => {
            return new Promise((resolve, reject) => {
                FB.login((res) => {
                    if (typeof (res.authResponse) === 'undefined') {
                        reject({ error: 'falha ao realizar login' });
                        return;
                    }
                    resolve(res);
                }, {
                        scope: CONSTANTS.get('USER_PERMISSIONS')
                    });
            });
        };
        this.loginStatus = () => {
            return new Promise((resolve, reject) => {
                FB.getLoginStatus((res) => {
                    if (response.status !== 'connected') {
                        reject({ error: 'Usuário não conectado.' });
                        return;
                    }
                    resolve(res);
                });
            });
        };
        this.myPages = (asd) => {
            var self = this;
            return new Promise((resolve, reject) => {
                FB.api(`/${self.userId}/assigned_pages`, (res) => {
                    console.log(res);
                });
            });
        };

        this.init()
    }
}

/*
function publish() {
  FB.api(
      `/${window.localStorage.getItem('userID')}/feed`,
      "POST", {
          "message": "This is a test message"
      },
      function (response) {
        console.log(response)
      }
  );
}

function shareContent() {
  FB.ui({
    method: 'share',
    href: 'https://developers.facebook.com/docs/',
  }, function(response, res1) {
    console.log(response, res1)
  });
}
*/