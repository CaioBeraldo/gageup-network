import { Module } from '@nestjs/common';
import { UsuariosModule } from 'usuarios/usuarios.module';
import { AuthModule } from 'auth/auth.module';
import { PaginasModule } from 'paginas/paginas.module';
import { PublicacoesModule } from 'publicacoes/publicacoes.module';

@Module({
  imports: [
    UsuariosModule,
    PaginasModule,
    PublicacoesModule,
    AuthModule,
  ],
  providers: [],
  controllers: [],
})
export class AppModule {}
