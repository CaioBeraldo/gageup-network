# gun

## Description

GageUp Network


## Links

[https://github.com/nestjs/nest/issues/113](Configuração SSL NestJs)
[https://stackoverflow.com/questions/21397809/create-a-trusted-self-signed-ssl-cert-for-localhost-for-use-with-express-node](Geração de SSL para localhost)

[http://programmerblog.net/nodejs-https-server/](Geração de SSL para localhost)
[https://thycotic.force.com/support/s/article/Trusting-an-SSL-Certificate-on-a-Client-Machine](Autorização de certificado para localhost)
[https://medium.freecodecamp.org/how-to-get-https-working-on-your-local-development-environment-in-5-minutes-7af615770eec](Configuração SSL local Seguro)

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

npm i -g nodemon 		// 1.18.8
npm i -g ts-node 		// 7.0.1
npm i -g typescript		// 3.2.2
npm i -g node-pre-gyp	// 0.12.0

