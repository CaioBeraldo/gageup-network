# GUN - Front-End

## Graph API

[Graph API](https://developers.facebook.com/docs/graph-api)

## Exemplos

``` js
/**
 * Fluxo de acesso:
 *  - Login com o facebook (como usuário).
 *  - Listar páginas vinculadas ao usuário (apenas para businessUsers ou systemUsers)
 *  - Buscar informações específicas das páginas.
 */

FB.login((res) => {
  console.log(res.authResponse)
}, {
  scope: 'user_likes,user_posts,email,publish_to_groups,publish_pages,manage_pages'
});

/*
res.status:
  connected - a pessoa está conectada ao Facebook e conectou-se ao seu aplicativo.
  not_authorized - a pessoa está conectada ao Facebook, mas não se conectou ao seu aplicativo..
  unknown - a pessoa não está conectada ao Facebook, então não é possível saber se ela se conectou ao seu aplicativo ou se 
    FB.logout() foi chamado antes e, portanto, não pode se conectar ao Facebook

Se estiver conectado:
  authResponse será incluído se o status for connected e for formado pelo seguinte:
  accessToken - contém um token de acesso para a pessoa que está usando o aplicativo.
  expiresIn - Indica o tempo de UNIX quando o token expira e precisa ser renovado.
  signedRequest - um parâmetro assinado, contendo informações sobre a pessoa que está utilizando o aplicativo.
  userID - é o ID da pessoa que está usando o aplicativo.
*/
FB.getLoginStatus(res => {
  console.log(res.status)
});


// BusinessUser / SysUser

// {userID}/assigned_pages
// consultar páginas de um usuário.
// https://developers.facebook.com/docs/marketing-api/businessmanager/systemuser/
// https://business.facebook.com/settings/pages/287739191956252?business_id=252653822256116
FB.api('/102956303996155/assigned_pages', (res) => {
  console.log(res);
});

// {pageID}
// consultar informações de uma página específica.
// https://developers.facebook.com/docs/graph-api/reference/page/
FB.api(
  "/GageUpNetwork", { fields: 'id,about,attire,business,category,category_list,context,cover,description,description_html,display_subtext,engagement,fan_count,current_location,general_info,hours,is_always_open,is_community_page,is_permanently_closed,is_published,link,location,mission,page_token,phone,username,store_number,verification_status,website,whatsapp_number,were_here_count' },
  function (res) {
    console.log(res);
  }
);

// {pageID}/feed
// listar as publicações de uma página.
// https://developers.facebook.com/docs/graph-api/reference/page/
FB.api(
  "/GageUpNetwork/feed", {
    limit: 10 // exibir 10 últimas publicações
  }, function (res) {
    console.log(res);
  }
);

// {pageId}
// obter token de acesso como página.
// https://developers.facebook.com/docs/pages/access-tokens/?locale=pt_BR
FB.api(
  "/GageUpNetwork/", {
    fields: "access_token"
  }, function (res) {
    console.log(res);
  }
);

// {userID}|{pageID}_{objectID}/likes
// visualizar curtidas de uma publicação
FB.api(
  "/287739191956252_287743685289136/likes?access_token=EAADGuKw1TFwBAJpdZAnypr8o1CE3oWhhE0VX556RNLPzGbOhobgXSMdQomZB9ZBTOfoFEHZCSpqiMZBSdogKeAUivZBzUL0FfiuJovNRpq7pwQuaKzlumRuOkZAUCrPzHfGkNgyg9jrEyojDDg4tWAnKtmKIxZBd0ldO08QWa5yEhz7Hc9NT7p8nJdviZAmUqOMpp2QnuIWKgZCwZDZD", function (res) {
    console.log(res);
  }
);

// {userID}|{pageID}_{objectID}/likes
// cutir uma publicação como página.
FB.api(
  "/287739191956252_287743685289136/likes?access_token=EAADGuKw1TFwBAJpdZAnypr8o1CE3oWhhE0VX556RNLPzGbOhobgXSMdQomZB9ZBTOfoFEHZCSpqiMZBSdogKeAUivZBzUL0FfiuJovNRpq7pwQuaKzlumRuOkZAUCrPzHfGkNgyg9jrEyojDDg4tWAnKtmKIxZBd0ldO08QWa5yEhz7Hc9NT7p8nJdviZAmUqOMpp2QnuIWKgZCwZDZD", 'POST', function (res) {
    console.log(res);
  }
);

// {userID}|{pageID}_{objectID}/comments
// comentar em uma publicação como página.
FB.api(
  "/287739191956252_287743685289136/comments?access_token=EAADGuKw1TFwBAJpdZAnypr8o1CE3oWhhE0VX556RNLPzGbOhobgXSMdQomZB9ZBTOfoFEHZCSpqiMZBSdogKeAUivZBzUL0FfiuJovNRpq7pwQuaKzlumRuOkZAUCrPzHfGkNgyg9jrEyojDDg4tWAnKtmKIxZBd0ldO08QWa5yEhz7Hc9NT7p8nJdviZAmUqOMpp2QnuIWKgZCwZDZD", {
    message: 'Hello world'
  }, 'POST', function (res) {
    console.log(res);
  }
);

// buscar posts do usuário.
window.FB.api(`/me/posts`, (res) => {
	console.log(res)
});

// https://www.facebook.com/${pageId}/posts/{objectId}
// compartilhar uma publicação
FB.ui({
  method: 'share',
  href: `https://www.facebook.com/${pageId}/posts/287743685289136`,
}, function(response, res1) {
  console.log(response, res1)
});

```

## Variaveis de Ambiente

[Code Burst](https://codeburst.io/how-to-easily-set-up-node-environment-variables-in-your-js-application-d06740f9b9bd)
[Umbler Cloud](https://help.umbler.com/hc/pt-br/articles/115001793863-Node-JS-na-Umbler#variaveisDeAmbiente)

## git remote add umbler [Git](https://tatooine.deploy.umbler.com/0ko48pwe/app-gageup-com-br.git)

git push umbler master

## npm install -g typescript (tcs)
