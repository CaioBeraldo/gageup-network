
export const handleResponse = (res) => {
  if (!res.body) {
    return res;
  }

  return res.json();
}

export const handleErrors = (data) => {
  return new Promise(async (resolve, reject) => {
    if (!data) {
      reject();
    }

    if (data.error) {
      reject(data);
    }

    if (!data.status) {
      resolve(data);
    }

    switch (data.status) {
      case 200: // ok
        resolve(data);
        break;
      case 202: // accepted
        resolve(data);
        break;
      case 302: // found
        resolve(data);
        break;
      default:
        data = await handleResponse(data);
        reject(data);
        break;
    }
  })
}
