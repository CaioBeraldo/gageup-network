import { isEmpty } from "../utils";
import { handleErrors, handleResponse } from ".";
// import { Store } from '../store/stores';

/**
 * Constantes
 */

const ENDPOINT = process.env.URL_API || 'http://localhost:3001';
const ENDPOINT_AUTH = `${ENDPOINT}/auth`;
const ENDPOINT_AUTH_SIGNIN = `${ENDPOINT_AUTH}/signin`
const ENDPOINT_USUARIOS = `${ENDPOINT}/usuarios`;
const ENDPOINT_PAGINAS = `${ENDPOINT}/paginas`;
const ENDPOINT_PUBLICACOES = `${ENDPOINT}/publicacoes`;

// API response: { result: String, data: {}, error: {} }
// const ENDPOINT_RESULT_SUCCESS = 'success';
// const ENDPOINT_RESULT_ERROR = 'error';
// const ENDPOINT_RESULT_UNAUTHORIZED = 'unauthorized';

const PUT = { method: 'PUT' }
const POST = { method: 'POST' }
const GET = { method: 'GET' }
// const DELETE = { method: 'DELETE' }

/**
 * Funções auxiliares
 */

const makeAuthorization = () => {
  const token = sessionStorage.getItem('token');

  if (isEmpty(token)) {
    return;
  }

  return {
    'Authorization': `Bearer ${token}`
  }
}

const makeBody = (data) => {
  if (isEmpty(data)) {
    return;
  }

  return { body: JSON.stringify(data) }
}

const makeHeaders = () => {
  return { 
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      ...makeAuthorization(),
    }
  };
}

const makeOptions = (method, body) => {
  return {
    ...method,
    ...makeHeaders(),
    ...makeBody(body),
  }
}

const fetchAPI = (url, method, body) => {
  return fetch(url, makeOptions(method, body))
    .then(handleErrors)
    .then(handleResponse)
}

/**
 * Funções externas
 */

export const criarConta = async (usuario) => {
  return await new Promise((resolve, reject) => {
    fetchAPI(ENDPOINT_AUTH, POST, usuario)
      .then(resolve)
      .catch(reject);
  });
}

export const criarSenha = async (usuario) => {
  return await new Promise((resolve, reject) => {
    fetchAPI(ENDPOINT_AUTH, PUT, usuario)
      .then(resolve)
      .catch(reject);
  });
}

export const gerarToken = async (usuario) => {
  return await new Promise((resolve, reject) => {
    fetchAPI(ENDPOINT_AUTH_SIGNIN, POST, usuario)
      .then(resolve)
      .catch(reject);
  });
}

export const usuarioExiste = async (id) => {
  return await new Promise((resolve, reject) => {
    fetchAPI(`${ENDPOINT_AUTH}/${id}`, GET)
      .then(resolve)
      .catch(reject);
  });
}

export const adicionarPagina = (pagina) => {
  return new Promise((resolve, reject) => {
    fetchAPI(ENDPOINT_PAGINAS, POST, pagina)
      .then(resolve)
      .catch(reject);
  });
}

export const engajarPublicacao = async (publicacao) => {
  const _publicacao = await publicacao;
  return new Promise((resolve, reject) => {
    fetchAPI(ENDPOINT_PUBLICACOES, POST, _publicacao)
      .then(resolve)
      .catch(reject);
  });
}

export const validarToken = async () => {
  return new Promise((resolve, reject) => {
    fetchAPI(ENDPOINT_USUARIOS, GET)
      .then(resolve)
      .catch(reject);
  });
}

export const getTeste = async () => {
  return await new Promise((resolve, reject) => {
    fetchAPI(ENDPOINT_USUARIOS, GET)
      .then(resolve)
  });
}
