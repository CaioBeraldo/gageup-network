import { isEmpty } from '../utils';
import { handleErrors, handleResponse } from '.';

/**
 * Constantes
 */

const STATUS_CONNECTED = 'connected';
const FB_FIELDS = { fields: 'name,email,id' };
const FB_SCOPE = {
  scope: atob('dXNlcl9saWtlcyx1c2VyX3Bvc3RzLGVtYWlsLHB1Ymxpc2hfdG9fZ3JvdXBzLHB1Ymxpc2hfcGFnZXMsbWFuYWdlX3BhZ2Vz'),
  auth_type: 'rerequest',
}

/**
 * Funções auxiliares
 */
/*
const resolveToUser = (obj) => {
  return {
    userID: obj.id,
    nome: obj.name,
    email: obj.email,
  }
}
*/
const resolveToError = (message) => {
  return { error: message }
}

const fetchAPI = (url, data) => {
  return new Promise((resolve, reject) => {
    window.FB.api(url, data, (res) => {
      resolve(res);
    })
  })
  .then(handleErrors)
  .then(handleResponse);
}

/**
 * Funções externas
 */

export const init = (maxTries) => {
  return new Promise(async (resolve, reject) => {
    setTimeout(() => {
      this.tries++;
      
      if (this.tries >= (maxTries || this.maxTries)) {
        resolve(false);
      }

      if (!window.FB) {
        init().then(resolve).catch(resolve);
      }

      resolve(true)
    }, 1000);
  });
}

export const doLogin = () => {
  return new Promise((resolve) => {
    window.FB.login((res) => resolve(!isEmpty(res.authResponse)), { ...FB_SCOPE });
  });
}

export const getLoginStatus = () => {
  return new Promise(resolve => {
    window.FB.getLoginStatus(res => {
      resolve((res.status === STATUS_CONNECTED))
    });
  });
}

export const getUserInfo = () => {
  return new Promise((resolve) => {
    window.FB.api(`/me`, { ...FB_FIELDS }, (res) => {
      if (isEmpty(res)) {
        resolve(resolveToError('Erro ao buscar informações do usuário.'));
      }

      if (res.error) {
        resolve(resolveToError(res.error.message))
      }

      resolve(res);
    });
  });
}

export const getPageInfo = (page) => {
  return new Promise((resolve, reject) => {
    window.FB.api(
      `/${page}`, { fields: `id,about,attire,business,category,category_list,
      context,cover,description,description_html,display_subtext,engagement,
      fan_count,current_location,general_info,hours,is_always_open,is_community_page,
      is_permanently_closed,is_published,link,location,mission,page_token,phone,
      username,store_number,verification_status,website,whatsapp_number,were_here_count`
    }, resolve);
  });
}

export const loginWithFacebook = () => {
  return new Promise((resolve, reject) => {
    getLoginStatus().then(loggedIn => {
      if (!loggedIn) {
        doLogin().then(getUserInfo).then(resolve).catch(reject);
      }

      getUserInfo().then(resolve).catch(reject);
    })
  });
}

export const getPost = (objectId, postId) => {
  return new Promise((resolve, reject) => {
    fetchAPI(`/${objectId}_${postId}`)
      .then(resolve)
      .catch(reject)
  });
}

