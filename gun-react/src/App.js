import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Header } from './components/Header';
import { Footer } from './components/Footer';
import { Body } from './components/Body';
import { setEmail, setToken } from './store/actions';
import { validarToken } from './apis/gageup';
import './App.css';

class App extends Component {
  componentDidMount() {
    validarToken().then(() => {
      const token = sessionStorage.getItem('token');
      const email = sessionStorage.getItem('email');

      this.props.setToken(token);
      this.props.setEmail(email);
    }).catch(() => {
      sessionStorage.setItem('token', undefined);
      sessionStorage.setItem('email', undefined);
    });
  }

  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Header />
          <Body />
          <Footer />
        </div>
      </BrowserRouter>
    )
  }
}

const mapStateToProps = (state) => ({ })

const mapDispatchToProps = (dispatch) => {
  return {
    ...setEmail(dispatch),
    ...setToken(dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
