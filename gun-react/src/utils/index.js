
export const isEmpty = (value) => (!value) || (typeof (value) === 'undefined') || (value === '');

export const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));
