import React from 'react';
import { Link } from 'react-router-dom';
import { connectToStore } from '../store';
import { isEmpty } from '../utils';

const Navigator = ({ token }) => {

  const renderRoutes = () => {
    if (isEmpty(token)) {
      return (
        <nav>
          <Link to="/entrar">Entrar</Link>
        </nav>
      );
    }

    return (
      <nav>
        <Link to="/minhasPaginas">Minhas Paginas</Link>
        <Link to="/alterarSenha">Alterar Senha</Link>
        <Link to="/sair">Sair</Link>
      </nav>
    );
  }

  return (
    <div>
      {renderRoutes()}
    </div>
  )
}

const mapStateToProps = (state) => ({
  token: state.gageupReducer.token,
})

export default connectToStore(Navigator, mapStateToProps);
