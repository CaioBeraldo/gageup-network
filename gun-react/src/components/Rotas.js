import React from 'react';
import { Route } from 'react-router-dom';
import AlterarSenha from './auth/AlterarSenha';
import MinhasPaginas from './usuarios/MinhasPaginas';
import EngajarPublicacao from './usuarios/EngajarPublicacao';
import Login from './auth/Login';
import Sair from './auth/Sair';

export const Routes = () => (
  <div>
    <Route path="/entrar" render={() => <Login /> } />
    <Route path="/minhasPaginas" render={() => <MinhasPaginas />} />
    <Route path="/engajarPublicacao/:page_id" render={() => <EngajarPublicacao />} />
    <Route path="/sair" render={() => <Sair />} />
    <Route path="/alterarSenha" render={() => <AlterarSenha />} />
  </div>
);
