import React, { Component } from 'react';
import { loginWithFacebook } from '../../apis/facebook';
import { criarSenha } from '../../apis/gageup';

class CriarSenha extends Component {
  state = {
    error: '',
    message: '',
  };

  componentDidMount() {
    document.getElementById('criar-senha').addEventListener('click', this.criarSenha)
  }

  criarSenha = async () => {
    const password = document.getElementById('password').value;
    const passwordConfirmation = document.getElementById('password-confirmation').value;

    if (password !== passwordConfirmation) {
      this.setState({ error: 'Senhas não conferem.' })
      return;
    }

    loginWithFacebook().then(async (usuario) => {
      usuario.password = password;
      usuario = await criarSenha(usuario);
      console.log(usuario)
    })
    // const loginStatus = await this.props.FA.getLoginStatus();
    // const userInfo = await getUserInfo();

    // if (loginStatus.error || userInfo.error) {
    //   this.setState({ ...loginStatus || userInfo, message: '' });
    //   return;
    // }

  }

  render() {
    return (
      <div>
        <input id="password" type="password"></input>
        <input id="password-confirmation" type="password"></input>
        <button id="criar-senha">Confirmar</button>
        <span>{this.state.error || this.state.message || ''}</span>
      </div>
    );
  }
}

export default CriarSenha;