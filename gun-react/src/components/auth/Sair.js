import React, { Component } from 'react';
import { connectToStore } from '../../store';
import { setEmail, setToken } from '../../store/actions';

class Sair extends Component {
  state = {
    logout: false,
  }

  componentDidMount() {
    sessionStorage.setItem('token', undefined);
    sessionStorage.setItem('email', undefined);
    this.props.setEmail();
    this.props.setToken();

    setTimeout(() => {
      this.setState({ logout: true });
    }, 5000)

    setTimeout(() => {
      this.props.history.push('/');
    }, 6000)

  }

  renderSair() {
    if (!this.state.logout) {
      return <span>Aguarde enquanto desconectamos...</span>;
    }

    return <span>Logout realizado com sucesso!</span>;
  }

  render() {
    return (
      <div>
        {this.renderSair()}
      </div>
    );
  }

};

const mapStateToProps = (state) => ({})

const mapDispatchToProps = (dispatch) => ({
  ...setEmail(dispatch),
  ...setToken(dispatch)
})

export default connectToStore(Sair, mapStateToProps, mapDispatchToProps);