import React, { Component } from 'react';
import { connectToStore } from '../../store';
import { getTeste } from '../../apis/gageup';

class AlterarSenha extends Component {
  componentDidMount() {
    console.log(this.props)
    document.getElementById('solicitar-alteracao').addEventListener('click', this.consultar);
  }

  async consultar() {
    const data = await getTeste();
    console.log(data);
  }

  async deletar() {
    const token = sessionStorage.getItem('token');
    await fetch('http://localhost:3001/usuarios/delete', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
      },
    });
  }

  render() {
    return (
      <div>
        <button id="solicitar-alteracao">Solicitar alteração de senha</button>
      </div>
    );
  }
}

export default connectToStore(AlterarSenha);
