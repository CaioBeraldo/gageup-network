import React from 'react';
import { loginWithFacebook } from '../../apis/facebook';
import { criarConta, usuarioExiste } from '../../apis/gageup';
import { isEmpty } from '../../utils';
import { connectToStore } from '../../store';
import { setEmail } from '../../store/actions';

const LoginComFacebook = ({ setEmail }) => {

  const login = (e) => {
    e.preventDefault();

    loginWithFacebook().then(userInfo => {
      if (isEmpty(userInfo)) {
        setEmail();
        return;
      }

      usuarioExiste(userInfo.id).then(resultado => {
        if (resultado.existe) {
          setEmail(resultado.email);
          return;
        }

        criarConta(userInfo).then(conta => setEmail(conta.email))
      })
    })
  }

  return (
    <form onSubmit={login}>
      <input type="submit" value="Entrar com o Facebook" />
    </form>
  );
}

const mapStateToProps = (state) => ({ })

const mapDispatchToProps = (dispatch) => {
  return {
    ...setEmail(dispatch),
  }
}

export default connectToStore(LoginComFacebook, mapStateToProps, mapDispatchToProps)
