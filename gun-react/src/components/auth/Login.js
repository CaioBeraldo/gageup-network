import React, { Component } from 'react';
import { connectToStore } from '../../store';
import CriarSenha from './CriarSenha';
import Entrar from './Entrar';
import LoginComFacebook from './LoginComFacebook';

class Login extends Component {

  componentDidMount() { }

  renderLogin() {
    if (!this.props.email) {
      return <LoginComFacebook />;
    }

    if (!this.props.token) {
      return <Entrar />;
    }

    console.warn('VERIFICAR SE SENHA JÁ FOI CRIADA OU NÃO.');
    return <CriarSenha />;
  }

  render() {
    return (
      <div>
        {this.renderLogin()}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  email: state.gageupReducer.email,
})

const mapDispatchToProps = (dispatch) => ({ })

export default connectToStore(Login, mapStateToProps, mapDispatchToProps);
