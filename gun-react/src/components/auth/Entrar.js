import React, { Component } from 'react';
import { gerarToken } from '../../apis/gageup';
import { setToken, setEmail } from '../../store/actions';
import { connectToStore } from '../../store';

class Entrar extends Component {
  componentDidMount() {
  }

  login = async (e) => {
    e.preventDefault();

    const password = document.getElementById('password').value;

    gerarToken({ email: this.props.email, password })
      .then(usuario => {
        sessionStorage.setItem('token', usuario.token);
        sessionStorage.setItem('email', usuario.email);
        this.props.setToken(usuario.token);
        this.props.setEmail(usuario.email);
        this.props.history.push('/');
      })
      .catch(console.error);
  }

  render() {
    return (
      <form onSubmit={this.login}>
        <input id="password" type="password" />
        <input type="submit" value="Entrar" />
      </form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    ...state.gageupReducer,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    ...setEmail(dispatch),
    ...setToken(dispatch),
  }
}

export default connectToStore(Entrar, mapStateToProps, mapDispatchToProps);
