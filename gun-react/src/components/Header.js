import React from 'react';
import logo from '../logo.svg';
import Navigator from './Navigator';

export const Header = () => (
  <header className="App-header">
    <img src={logo} className="App-logo" alt="logo" />
    <h1 className="App-title">Welcome to React</h1>
    
    <Navigator />
  </header>
);
