import React from 'react';

export const Erro = ({error}) => (
  <div>
    <span>{error}</span>
  </div>
);
