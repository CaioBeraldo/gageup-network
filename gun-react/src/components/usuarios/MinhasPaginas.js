import React from 'react';
import { connectToStore } from '../../store';
import AdicionarPagina from './paginas/AdicionarPagina';
import { PaginasList } from './paginas/PaginasList';

const MinhasPaginas = ({ paginas }) => {

  return (
    <div>
      <AdicionarPagina />
      <PaginasList {...paginas} />
    </div>
  );
}

const mapStateToProps = (state) => ({
  ...state.gageupReducer,
})
const mapDispatchToProps = (dispatch) => ({ })

export default connectToStore(MinhasPaginas, mapStateToProps, mapDispatchToProps)
