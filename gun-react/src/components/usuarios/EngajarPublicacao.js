import React, { Component } from 'react';
import { connectToStore } from '../../store';
import { getPost } from '../../apis/facebook';
import { engajarPublicacao } from '../../apis/gageup';

class EngajarPublicacao extends Component {
  componentDidMount() {
    console.log(this.props)
  }

  async carregarPublicacao (e) {
    e.preventDefault();

    const pageId = '287739191956252'; // this.props.match.params.page_id;
    const postId = '298854080844763';
    // const postId = document.getElementById('publicacao').value
    const post = await getPost(pageId, postId);
    const res = await engajarPublicacao(post);
    console.log(res);
  }

  render() {
    return (
      <div>
        <form onSubmit={this.carregarPublicacao}>
          <input type="submit" value="Carregar"></input>
        </form>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    ...state.gageupReducer,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connectToStore(EngajarPublicacao, mapStateToProps, mapDispatchToProps);
