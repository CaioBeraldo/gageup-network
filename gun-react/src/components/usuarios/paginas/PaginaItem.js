import React from 'react';
import { Link } from 'react-router-dom';

export const PaginasItem = ({ pagina }) => {
  return (
    <li>
      <Link to={`/engajarPublicacao/${pagina.id}`}>Página {pagina.id}</Link>
    </li>
  );
}
