import React from 'react';
import { PaginasItem } from './PaginaItem';

export const PaginasList = ({ paginas }) => {

  if (!paginas) {
    return <span>Nenhuma página cadastrada...</span>
  }

  return (
    <ul>
      {paginas.map(pagina => <PaginasItem {...pagina} />)}
    </ul>
  );
}
