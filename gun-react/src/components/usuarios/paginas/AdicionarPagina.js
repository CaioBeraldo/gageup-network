import React from 'react';
import { getPageInfo, getUserInfo } from '../../../apis/facebook';
import { adicionarPagina } from '../../../apis/gageup';
import { connectToStore } from '../../../store';
import { addPage } from '../../../store/actions';

const AdicionarPagina = ({ token, addPage }) => {

  const adicionar = async (e) => {
    e.preventDefault();

    const nome = document.getElementById('pagina').value;
    const usuario = await getUserInfo();
    const pagina = await getPageInfo(nome)

    if (pagina.error && pagina.error.type === "OAuthException") {
      // this.setState({ error: 'Você não tem permissão na página que está tentando acessar.' })
      console.warn(pagina);
      return;
    }

    const res = await adicionarPagina({ userID: usuario.userID, pagina });
    console.log(res);
    addPage(res);
  }

  return (
    <div>
      <form id="adicionar-pagina" onSubmit={adicionar}>
        <input id="pagina" type="text" />
        <input id="adicionar" type="submit" value="Adicionar página" />
      </form>
    </div>
  );
}

const mapStateToProps = (state) => ({
  ...state.gageupReducer,
})
const mapDispatchToProps = (dispatch) => ({
  ...addPage(dispatch)
})

export default connectToStore(AdicionarPagina, mapStateToProps, mapDispatchToProps)
