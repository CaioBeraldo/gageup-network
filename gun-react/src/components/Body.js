import React from 'react';
import { Routes } from './Rotas';

export const Body = () => {

  const renderRoutes = () => {
    /*
    return (<Carregando />);

    if (!this.state.loaded && !this.state.loading) {
      return (<Erro error={'Erro ao carregar APIs do Facebook.'} />);
    }
    */
   return <Routes />;
   /*
    if (!this.state.hasToken) {
      console.warn('não esta logado')
      return <Routes />;
    }

    console.log('esta logado')
    return <Routes />;
    */
  }

  return (
    <div>
      {renderRoutes()}
    </div>
  )
};
