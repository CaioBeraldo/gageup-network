import React from 'react';

export const Carregando = () => (
  <div>
    <span>Carregando...</span>
  </div>
);
