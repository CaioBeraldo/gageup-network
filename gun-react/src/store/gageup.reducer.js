import { SET_USER_EMAIL, SET_USER_LOGIN, ADD_PAGE, SET_USER_TOKEN } from "./actions";

const initialState = { };

export const gageupReducer = (state = initialState, payload) => {
  console.log(payload)
  switch (payload.type) {
    case SET_USER_EMAIL:
      return {
        ...state,
        email: payload.email,
      }
    case SET_USER_LOGIN:
      return {
        ...state,
        loggedIn: payload.loggedIn,
      }
    case SET_USER_TOKEN:
      return {
        ...state,
        token: payload.token,
      }
    case ADD_PAGE:
      return {
        ...state,
        paginas: [...state.paginas, payload.pagina],
      }
    default:
      return state;
  }
};
