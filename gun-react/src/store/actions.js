
/**
 * action types
 */

export const SET_USER_EMAIL = 'SET_USER_EMAIL';
export const SET_USER_LOGIN = 'SET_USER_LOGIN';
export const SET_USER_TOKEN = 'SET_USER_TOKEN'
export const ADD_PAGE = 'ADD_PAGE';

/*
* other constants
*/

/*​
export const VisibilityFilters = {
  SHOW_ALL: 'SHOW_ALL',
  SHOW_COMPLETED: 'SHOW_COMPLETED',
  SHOW_ACTIVE: 'SHOW_ACTIVE'
}
*/

/*
 * action creators
 */
/*
export const aguardarInicializacao = (maxTries) => (
  { type: AGUARDAR_INICIALIZACAO, maxTries }
);
*/

const setEmailAction = (payload) => ({ type: SET_USER_EMAIL, email: payload });

const setLoginAction = (payload) => ({ type: SET_USER_LOGIN, loggedIn: payload });

const addPageAction = (payload) => ({ type: ADD_PAGE, pagina: payload })

const setTokenAction = (payload) => ({ type: SET_USER_TOKEN, token: payload })


export const setEmail = (dispatch) => ({
  setEmail: (email) => dispatch(setEmailAction(email)),
})

export const setLogin = (dispatch) => ({
    setLogin: (login) => dispatch(setLoginAction(login)),
})

export const addPage = (dispatch) => ({
  addPage: (pagina) => dispatch(addPageAction(pagina)),
})

export const setToken = (dispatch) => ({
  setToken: (token) => dispatch(setTokenAction(token)),
})

// dispatch(aguardarInicializacao(maxTries))
// dispatch(addTodo(text))
// dispatch(completeTodo(index))
// const boundAddTodo = text => dispatch(addTodo(text))
// const boundCompleteTodo = index => dispatch(completeTodo(index))
