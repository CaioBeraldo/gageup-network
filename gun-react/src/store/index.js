import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';

export const connectToStore = (Component, mapState, mapDispatch) => {
  return connect(mapState, mapDispatch)(withRouter(Component))
}
