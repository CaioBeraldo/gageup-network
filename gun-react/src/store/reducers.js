import { gageupReducer } from './gageup.reducer';
import { combineReducers } from 'redux';

export const Reducers = combineReducers({ gageupReducer });
